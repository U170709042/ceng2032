.data

firt: .asciiz "Please enter an integer: "

second: .asciiz "\nPlease enter another integer: "

third: .asciiz "\nThe greatest common divisor is: "

.text
main:

	addi $a0,$zero,1071
	addi $a1,$zero,462
	jal gcdRec2
	addi $a0,$v0,0
	li $v0,1

	syscall
	li $v0,10

	syscall
	
	gcdRec1:
	A: beq $a0,$a1,done
	slt $t1,$a0,$a1
	bne $t1,$zero,B

	sub $a0,$a0,$a1
	j A

	B:sub $a1,$a1,$a0
	j A

	done:add $v0,$a0,$zero
	jr $ra
	
	gcdRec2:
	beq $a0,$a1,done2
	addi $sp,$sp,-4
	sw $ra,0($sp)
	slt $t0,$a0,$a1
	beq $t0,$zero,D

	sub $a1,$a1,$a0
	jal gcdRec2
	j C
	
	D:

	sub $a0,$a0,$a1
	jal gcdRec2

	C: lw $ra,0($sp)
	addi $sp,$sp,4
	jr $ra

	done2: add $v0,$a0,$zero
	jr  $ra
	
