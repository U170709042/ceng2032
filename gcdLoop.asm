.data


firstone: .asciiz "Please enter an integer: "

secondone: .asciiz "\nPlease enter another integer: "

thirdone: .asciiz "\nThe greatest common divisor is: "

.text
main:

addi $a0,$zero,1071
	addi $a1,$zero,462
	jal gcdLoop
	addi $a0,$v0,0
	li $v0,1

	syscall
	li $v0,10
	
	syscall
	gcdLoop: beq $a0, $a1, exit
	slt $t0, $a1, $a0
	bne $t0, $zero, L1
	
	sub $a1, $a1, $a0
	j gcdLoop
	L1: sub $a0, $a0, $a1
	j gcdLoop

	exit: add $v0, $a0, $zero
	jr $ra
